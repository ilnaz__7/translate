"""Данный модуль предназначен для разделенияз строки по словам"""
from typing import List
from tco import *
lst = List[str]
@with_continuations()
def rsplit_tail(sentence: str,seps: lst,keep_sep: bool=False,get_pos: bool=False,temp=[],begin=0,end=0,self=None) -> lst:
    """Функция разбивает строку на подстроки(слова). Возвращает список слов.

    Arguments:
        sentence {str} -- Строка, которую вы хотите разбить на слова.
        seps {lst} -- Список разделителей

    Keyword Arguments:
        keep_sep {bool} -- Если вы хотите разделить и разделители, то True (default: {False})
        get_pos {bool} -- если вы хотите узнать позиции слов(начало, конец), то True (default: {False})
        temp {list} -- Является накопителем результата (default: {[]})
        begin {int} -- Предназначена для get_pos, для выявляения начала слова(default: {0})
        end {int} -- [Предназначена для get_pos, для выявляения конца слова(default: {0})
        self {[type]} -- Аргумент для хвостовой рекурсии (default: {None})

    Returns:
        lst -- Возвращает список разделенных слов, тип - список. """
    if [1 for znak in seps if znak in sentence] != []:
        razd = min([sentence.find(i) for i in seps if i in sentence])
        end = end + razd
        return self(sentence[razd+1:],seps,keep_sep,get_pos,temp 
                + (([(sentence[:razd],begin,end-1)] if razd > 0 else []) if get_pos else ([sentence[:razd]] if razd > 0 else []))
                + ([sentence[razd]] if sentence[razd] != ' ' and keep_sep else []),end+1,end+1)        
    else:
        end = end + len(sentence)
        return (temp + [(sentence,begin,end-1)] if len(sentence) > 0 else temp) if get_pos else temp + [sentence] if len(sentence) > 0 else temp